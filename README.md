# BinarySearch

```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Введите кол-во элементов массиве : ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] d = new int[n];
            try
            {
                for (int j = 0; j < d.Length; ++j)
                {
                    Console.Write("{0} элемент массива ", j + 1);
                    d[j] = int.Parse(Console.ReadLine());
                }
            } catch
            {
                Console.WriteLine("Введите число!");
            }

            Array.Sort(d);
            Console.WriteLine("Отсортированный массив: ");
            int index = 0;
            Array.ForEach(d, x =>
            {
                Console.WriteLine("[{0}]: {1}", index++, x);
            });

            Console.Write("\nНайти: ");
            int key = int.Parse(Console.ReadLine());
            int i = BinarySearch(d, key, 0, d.Length - 1);
            if (i < d.Length)
                Console.WriteLine("Индекс искомого элемента: {0}", i);
            else
                Console.WriteLine("Элемент не найден");
            Console.ReadKey(true);

        }
        static int BinarySearch(int[] d, int key, int left, int right) 
        {
            int mid = left + (right - left) / 2; 
            if (left >= right)
                return -(1 + left);

            if (d[mid] == key) 
                return mid;

            else if (d[mid] > key) 
                return BinarySearch(d, key, left, mid);
            else
                return BinarySearch(d, key, mid + 1, right);
        }
    }
}
```